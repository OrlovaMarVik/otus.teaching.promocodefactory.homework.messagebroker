﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Service
{
    public interface IPromoCodeService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request);
    }
}
