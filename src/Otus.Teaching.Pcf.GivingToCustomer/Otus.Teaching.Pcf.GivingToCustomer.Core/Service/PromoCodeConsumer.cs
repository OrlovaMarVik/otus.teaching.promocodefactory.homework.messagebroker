﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Service
{
    public class PromoCodeConsumer: IConsumer<GivePromoCodeRequest>
    {
        readonly IPromoCodeService _promoCodeService;

        public PromoCodeConsumer(IPromoCodeService promoCodeService)
        {
            _promoCodeService = promoCodeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeRequest> context)
        {
            await _promoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}
