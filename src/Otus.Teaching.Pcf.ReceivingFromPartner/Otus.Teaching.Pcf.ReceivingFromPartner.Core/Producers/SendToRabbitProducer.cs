﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Threading.Tasks;
using PartnerPromoCode = Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain.PromoCode;
using ManagerBaseEntity = Otus.Teaching.Pcf.Administration.Core.Domain.BaseEntity;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core
{
    public class SendToRabbitProducer : ISendToRabbitProducer
    {
        readonly ISendEndpointProvider _sendEndpointProvider;

        public SendToRabbitProducer(ISendEndpointProvider sendEndpointProvider)
        {
            _sendEndpointProvider = sendEndpointProvider;
        }

        public async Task SendPromoCode(PartnerPromoCode promocode)
        {
            var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri("queue:promocode-queue"));
            await endpoint.Send(MapPromoCodeToCustomer(promocode));
        }

        public GivePromoCodeRequest MapPromoCodeToCustomer(PartnerPromoCode promoCode)
        {
            return new GivePromoCodeRequest()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PromoCodeId = promoCode.Id
            };
        }

        public async Task SendPartnerManagerId(ManagerBaseEntity baseEntity)
        {
            var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri("queue:manager-queue"));
            await endpoint.Send(baseEntity);
        }
    }
}
