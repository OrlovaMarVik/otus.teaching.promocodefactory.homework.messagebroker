﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ManagerBaseEntity = Otus.Teaching.Pcf.Administration.Core.Domain.BaseEntity;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core
{
    public interface ISendToRabbitProducer
    {
        Task SendPromoCode(PromoCode promoCode);

        Task SendPartnerManagerId(ManagerBaseEntity baseEntity);
    }
}
