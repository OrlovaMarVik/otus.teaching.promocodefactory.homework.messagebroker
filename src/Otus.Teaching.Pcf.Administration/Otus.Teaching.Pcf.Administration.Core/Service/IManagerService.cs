﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Service
{
    public interface IManagerService
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}
