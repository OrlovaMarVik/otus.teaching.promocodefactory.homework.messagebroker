﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Service;

namespace Otus.Teaching.Pcf.Administration.Core
{
    public class ManagerConsumer : IConsumer<BaseEntity>
    {
        readonly IManagerService _managerService;

        public ManagerConsumer(IManagerService managerService)
        {
            _managerService = managerService;
        }

        public async Task Consume(ConsumeContext<BaseEntity> context)
        {
            await _managerService.UpdateAppliedPromocodesAsync(context.Message.Id);
        }
    }
}
